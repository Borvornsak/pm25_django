import React from "react";
import { Layout } from "antd";

const { Footer } = Layout;

const AppFooter = () => {
  return (
    <Footer style={{ textAlign: "center" }}>
      Individual Study (Data Analytics) | 2017S2<br />PM2.5 data from
      http://aqmthai.com/
    </Footer>
  );
};

export default AppFooter;
